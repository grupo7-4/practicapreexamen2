const express = require("express");
const router = express.Router();
const bodyParse= require("body-parser");


router.get('/aviones',(req,res)=>{

    const valores={
        boleto:req.query.boleto,
        destino:req.query.destino,
        nombre:req.query.nombre,
        precio:req.query.precio,
        edad:req.query.edad,
        tipo:req.query.tipo,
        descuento:req.query.descuento,
        total:req.query.total,
        subtotal:req.query.subtotal
    }
   
    
    res.render('aviones.html',valores)


})

router.post("/aviones",(req,res)=>{
    const valores={ 
        boleto:req.body.boleto,
        destino:req.body.destino,
        nombre:req.body.nombre,
        precio:req.body.precio,
        edad:req.body.edad,
        tipo:req.body.tipo,
        descuento:req.body.descuento,
        total:req.body.total,
        subtotal:req.body.subtotal
    }
    res.render('aviones.html',valores);

})
module.exports=router;